/**
 * @file src/sheets-api.js
 * @author Gregor Adams <greg@pixelass.com>
 */

import google from 'googleapis'
import GoogleAuth from './google-auth'

const DEFAULTS = {
  scopes: ['https://www.googleapis.com/auth/spreadsheets.readonly'],
  serviceAccount: 'service_account.json',
  clientSecret: 'client_secret.json'
}

class SheetsAPI {
  /**
   * Creates a wrapper around google.sheets and google.auth
   * @param  {Object} [props={}] custom options
   * @return {function} returns the authorize method
   */
  constructor(props = {}) {
    this.options = {...DEFAULTS, ...props}
    this.sheets = google.sheets('v4')
    return this.authorize.bind(this)
  }

  /**
   * Authorize vie service account
   * @param  {object} [oauth2Client] allow using a previously authorized client
   * @param  {DefaultTransporter} oauth2Client.transporter
   * @param  {string} oauth2Client.clientId_
   * @param  {string} oauth2Client.clientSecret_
   * @param  {string} oauth2Client.redirectUri_
   * @param  {object} oauth2Client.credentials
   * @param  {string} oauth2Client.credentials.access_token
   * @param  {string} oauth2Client.credentials.token_type
   * @param  {number} oauth2Client.credentials.expiry_date
   * @param  {string} oauth2Client.credentials.refresh_token
   * @return {promise} returns a promise that resolves the methods
   */
  authorize(oauth2Client) {
    return new Promise((resolve, reject) => {
      if (oauth2Client) {
        // If oauth2Client exists
        // trust validity and continue
        this.oauth2Client = oauth2Client
        return resolve(this)
      }
      // Authorize with google
      const googleAuth = new GoogleAuth({
        scopes: this.options.scopes,
        serviceAccount: this.options.serviceAccount,
        clientSecret: this.options.clientSecret
      })
      return googleAuth().then(({oauth2Client}) => {
        // Set oauth2Client and resolve methods
        this.oauth2Client = oauth2Client
        return resolve(this.methods)
      }).catch(err => reject(err))
    })
  }

  /**
   * Public methods
   * @return {object}
   */
  get methods() {
    return {
      goToId: this.goToId.bind(this),
      goToSheet: this.goToSheet.bind(this),
      getRange: this.getRange.bind(this),
      batchGetRange: this.batchGetRange.bind(this),
      clearRange: this.clearRange.bind(this),
      setRange: this.setRange.bind(this),
      addRange: this.addRange.bind(this),
      getCell: this.getCell.bind(this),
      setCell: this.setCell.bind(this),
      batchGetCell: this.batchGetCell.bind(this),
      clearCell: this.clearCell.bind(this)
    }
  }

  /**
   * Set the spreadsheetId of the context
   * @param {string} spreadsheetId
   * @return {promise}
   */
  goToId(spreadsheetId) {
    this.spreadsheetId = spreadsheetId
    return Promise.resolve(this.methods)
  }

  /**
   * Set the sheetName of the context
   * @param {string} sheetName
   * @return {promise}
   */
  goToSheet(sheetName) {
    this.sheetName = sheetName
    return Promise.resolve(this.methods)
  }

  /**
   * Get cell values in a defined range
   * @param  {string} range A1 notation `A1:E7`
   * @return {promise} returns a promise that resolves the response
   */
  getRange(range) {
    return new Promise((resolve, reject) => {
      this.sheets.spreadsheets.values.get({
        auth: this.oauth2Client,
        spreadsheetId: this.spreadsheetId,
        range: `${this.sheetName}!${range}`
      }, (err, response) => err ? reject(err) : resolve(response))
    })
  }

  /**
   * Get cell values in a batch of defined ranges
   * @param  {array} ranges
   * @param  {string} ranges.<string> A1 notation `A1:E7` `Sheet1!A1:E7`
   * @param  {boolean} [sheetName=true] include sheet name from context
   * @return {promise} returns a promise that resolves the response
   */
  batchGetRange(ranges, sheetName = true) {
    return new Promise((resolve, reject) => {
      this.sheets.spreadsheets.values.batchGet({
        auth: this.oauth2Client,
        spreadsheetId: this.spreadsheetId,
        ranges: sheetName ? ranges.map(range => `${this.sheetName}!${range}`) : ranges
      }, (err, response) => err ? reject(err) : resolve(response))
    })
  }

  /**
   * Clear cell values in a defined range
   * @param  {string} range A1 notation `A1:E7`
   * @return {promise} returns a promise that resolves the response
   */
  clearRange(range) {
    return new Promise((resolve, reject) => {
      this.sheets.spreadsheets.values.clear({
        auth: this.oauth2Client,
        spreadsheetId: this.spreadsheetId,
        range: `${this.sheetName}!${range}`
      }, (err, response) => err ? reject(err) : resolve(response))
    })
  }

  /**
   * Get cell values in a defined range
   * @param  {string} range A1 notation `A1:E7`
   * @param  {array} values
   * @param  {array} values.<array>
   * @param  {string} values.<array>.<string>
   * @return {promise} returns a promise that resolves the response
   */
  setRange(range, values) {
    return new Promise((resolve, reject) => {
      this.sheets.spreadsheets.values.update({
        auth: this.oauth2Client,
        spreadsheetId: this.spreadsheetId,
        range: `${this.sheetName}!${range}`,
        valueInputOption: 'RAW',
        includeValuesInResponse: true,
        resource: {values}
      }, (err, response) => err ? reject(err) : resolve(response))
    })
  }

  /**
   * Add a new range after defined range
   * @param  {string} range A1 notation `A1:E7`
   * @param  {array} values
   * @param  {array} values.<array>
   * @param  {string} values.<array>.<string>
   * @return {promise} returns a promise that resolves the response
   */
  addRange(range, values) {
    return new Promise((resolve, reject) => {
      this.sheets.spreadsheets.values.append({
        auth: this.oauth2Client,
        spreadsheetId: this.spreadsheetId,
        range: `${this.sheetName}!${range}`,
        valueInputOption: 'RAW',
        includeValuesInResponse: true,
        resource: {values}
      }, (err, response) => err ? reject(err) : resolve(response))
    })
  }

  /**
   * Set the value of a cell
   * @param  {string} cell A1 notation `A1`
   * @param  {string} value
   * @return {promise} returns a promise that resolves the response
   */
  setCell(cell, value) {
    return new Promise((resolve, reject) => {
      this.sheets.spreadsheets.values.update({
        auth: this.oauth2Client,
        spreadsheetId: this.spreadsheetId,
        range: `${this.sheetName}!${cell}`,
        valueInputOption: 'RAW',
        includeValuesInResponse: true,
        resource: {values: [[value]]}
      }, (err, response) => err ? reject(err) : resolve(response))
    })
  }

  /**
   * Alias for getRange
   * @param {string} cells
   * @return {promise}
   */
  getCell(cell) {
    return this.getRange(cell)
  }

  /**
   * Alias for batchGetRange
   * @param {array} cells
   * @param {boolean} [sheetName]
   * @return {promise}
   */
  batchGetCell(cells, sheetName = true) {
    return this.batchGetRange(cells, sheetName)
  }

  /**
   * Alias for clearRange
   * @param {string} cell
   * @return {promise}
   */
  clearCell(cell) {
    return this.clearRange(cell)
  }
}

export default SheetsAPI
