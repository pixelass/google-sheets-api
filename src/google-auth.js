/**
 * @file src/google-auth.js
 * @author Gregor Adams <greg@pixelass.com>
 */

import fs from 'fs'
import path from 'path'
import GoogleAuthLibrary from 'google-auth-library'

const auth = new GoogleAuthLibrary()

/**
 * A wrapper around google JWT authorization.
 *
 * Uses [Google Service account](https://developers.google.com/identity/protocols/OAuth2ServiceAccount)
 *
 * Gives your application the authority to make API calls as users in your domain (to "impersonate" users).
 * When you prepare to make authorized API calls, you specify the user to impersonate.
 *
 * @memberOf GoogleAuth
 * @param  {object} account
 * @param  {string} account.client_email
 * @param  {string} account.private_key
 * @param  {array} scopes
 * @param  {string} scopes.<string>
 * @return {object} returns JWT client
 */
const jwtAuth = (account, scopes) =>
  new auth.JWT(account.client_email, null, account.private_key, scopes, null)

const DEFAULTS = {
  scopes: ['https://www.googleapis.com/auth/spreadsheets.readonly'],
  clientSecret: 'client_secret.json',
  serviceAccount: 'service_account.json'
}

class GoogleAuth {
  /**
   * Creates a wrapper around google-auth-library
   * @param  {Object} [props={}] custom options
   * @return {function} returns the `init` method
   */
  constructor(props = {}) {
    this.options = {...DEFAULTS, ...props}
    this.clientSecret = path.join(__dirname, this.options.clientSecret)
    this.serviceAccount = path.join(__dirname, this.options.serviceAccount)
    return this.init.bind(this)
  }

  /**
   * Get client secret then authorize using a service account.
   * @return {promise}
   */
  init() {
    return new Promise((resolve, reject) => {
      fs.readFile(this.clientSecret, (err, content) => {
        if (err) {
          return reject(err)
        }
        const credentials = JSON.parse(content)
        return this.authorize({credentials})
          .then(resolve)
          .catch(reject)
      })
    })
  }

  /**
   * Authorize using the service account
   * @param  {object} options.credentials
   * @param  {string} options.credentials.client_email
   * @param  {string} options.credentials.private_key
   * @return {promise}
   */
  authorize({credentials}) {
    return new Promise((resolve, reject) => {
      fs.readFile(this.serviceAccount, (err, content) => {
        if (err) {
          return reject(err)
        }
        // Create JWT client from serviceAccount
        const serviceAccount = JSON.parse(content)
        const jwtClient = jwtAuth(serviceAccount, this.options.scopes)
        return jwtClient.authorize((err, token) => {
          if (err) {
            return reject(err)
          }
          // Create oauth2Client from credentials and use serviceAccount token
          const oauth2Client = new auth.OAuth2(credentials.client_id, credentials.client_secret, credentials.redirect_uris)
          oauth2Client.credentials = token
          return resolve({oauth2Client})
        })
      })
    })
  }
}

export default GoogleAuth
