/**
 * @file src/helpers.js
 * @module helpers
 * @author Gregor Adams <greg@pixelass.com>
 */

import camelCase from 'camel-case'

/**
 * Helper to wrap methods and return
 * @param  {object} methods
 * @param  {object} props
 * @param  {string} props.type
 * @param  {string|array} props.target
 * @param  {string|array} props.data
 * @param  {function} [props.callback]
 * @return {promise} returns a promise that resolves the methods
 */
const defineType = (methods, props) => {
  const options = {
    callback() {},
    ...props
  }
  return new Promise((resolve, reject) =>
    methods[options.type](options.target, options.data).then(response => {
      options.callback(response)
      return resolve(methods)
    }).catch(err => reject(err))
  )
}

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const getRange = (methods, target, callback = x => x) =>
  defineType(methods, {type: 'getRange', target, callback})

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const clearRange = (methods, target, callback = x => x) =>
  defineType(methods, {type: 'clearRange', target, callback})

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const getCell = (methods, target, callback = x => x) =>
  defineType(methods, {type: 'getCell', target, callback})

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const clearCell = (methods, target, callback = x => x) =>
  defineType(methods, {type: 'clearCell', target, callback})

/**
 * @param  {object} methods
 * @param  {array} target
 * @param  {boolean} data
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const batchGetCell = (methods, target, data, callback = x => x) =>
  defineType(methods, {type: 'batchGetCell', target, data, callback})

/**
 * @param  {object} methods
 * @param  {array} target
 * @param  {boolean} data
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const batchGetRange = (methods, target, data, callback = x => x) =>
  defineType(methods, {type: 'batchGetRange', target, data, callback})

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {array} data
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const setRange = (methods, target, data, callback = x => x) =>
  defineType(methods, {type: 'setRange', target, data, callback})

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {string} data
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const setCell = (methods, target, data, callback = x => x) =>
  defineType(methods, {type: 'setCell', target, data, callback})

/**
 * @param  {object} methods
 * @param  {string} target
 * @param  {array} data
 * @param  {function} callback
 * @return {promise} returns a promise that resolves the methods
 */
const addRange = (methods, target, data, callback = x => x) =>
  defineType(methods, {type: 'addRange', target, data, callback})

/**
 * Converts arrays to objects
 * @param  {array} rows
 * @param  {array} [header=rows[0]] set fixed headers. If not set row 0 is used
 * @return {object}
 */
const toJSON = (rows, header = rows[0]) =>
  rows.filter((x, i) => header || (i > 0))
    .map(item => {
      const obj = {}
      header.forEach((prop, i) => {
        obj[camelCase(prop)] = item[i]
      })
      return obj
    })

export {
  getRange,
  clearRange,
  getCell,
  clearCell,
  batchGetRange,
  batchGetCell,
  addRange,
  setRange,
  setCell,
  toJSON
}
