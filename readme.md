# Google sheets API

node.js API for google sheets

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://raw.githubusercontent.com/pixelass/google-sheets-api/master/LICENSE)
[![GitHub issues](https://img.shields.io/github/issues/pixelass/google-sheets-api.svg?style=flat-square)](https://github.com/pixelass/google-sheets-api/issues)
[![Coveralls](https://img.shields.io/coveralls/pixelass/google-sheets-api.svg?style=flat-square)](https://coveralls.io/github/pixelass/google-sheets-api)
[![bitHound](https://img.shields.io/bithound/code/github/pixelass/google-sheets-api.svg?style=flat-square)](https://www.bithound.io/github/pixelass/google-sheets-api)
[![bitHound](https://img.shields.io/bithound/devDependencies/github/pixelass/google-sheets-api.svg?style=flat-square)](https://www.bithound.io/github/pixelass/google-sheets-api)

[![Babel](https://img.shields.io/badge/babel-stage--0-f5da55.svg?style=flat-square)](http://babeljs.io/docs/plugins/preset-stage-0/)
[![code style xo](https://img.shields.io/badge/code_style-XO-64d8c7.svg?style=flat-square)](https://github.com/sindresorhus/xo)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-44aa44.svg?style=flat-square)](https://github.com/conventional-changelog/standard-version)
[![test ava](https://img.shields.io/badge/test-🚀_AVA-0e1d5c.svg?style=flat-square)](https://github.com/avajs/ava)

[![yarn](https://img.shields.io/badge/yarn-friendly-2c8ebb.svg?style=flat-square)](https://yarnpkg.com/)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-44aa44.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)

<!-- toc -->

- [Links](#links)
- [Developing](#developing)
- [Testing](#testing)
- [Create Service Account](#create-service-account)
- [Create oAuth Client](#create-oauth-client)
- [Basic Usage](#basic-usage)
- [Methods](#methods)
- [Helpers](#helpers)
- [Options](#options)

<!-- tocstop -->

## Links
* [Documentation](https://pixelass.github.io/google-sheets-api/api/)


## Developing

* `run babel`: generates lib from source
* `run lint`: lints JavaScript files
* `run release`: release new version using "standard-version"

## Testing

Tests are currently not implemented.

## Create Service Account

1. Go to the [Google Developer console](https://console.developers.google.com/).
1. Select Credentials in the left sidebar.
1. Click the blue button "Create Credentials".
1. Choose "Service account key".
1. Create Service account or choose existing.
1. Choose "JSON".
1. Click Create.
1. Your JSON key file will be downloaded to your machine **(THIS IS THE ONLY COPY!)**
1. Save this file and use it as `service_account.json`
1. Share your spreadsheet with this user (email can be found in the JSON as `client_email`)

Your application now has the authority to make API calls as users in your domain (to "impersonate" users).
When you prepare to make authorized API calls, you specify the user to impersonate.

## Create oAuth Client
1. Go to the [Google Developer console](https://console.developers.google.com/).
1. Select Credentials in the left sidebar.
1. Click the blue button "Create Credentials".
1. Choose "oAuth client ID".
1. Choose "Other".
1. Enter a name.
1. Click Create.
1. You can download the data from your Project page in the [Google Developer console](https://console.developers.google.com/)
1. Save this file and use it as `client_secret.json`

## Basic Usage

```js
import SheetsAPI from 'node-google-sheets-api'

const sheetsAPI = new SheetsAPI()

sheetsAPI()
  .then(sheets => sheets.goToId('<SHEET_ID>'))
  .then(sheets => sheets.goToSheet('<SHEET_NAME>'))
  .then(sheets => sheets.getRange(sheets, 'A2:E10'))
  .then(response => {
    console.log(reponse.values)
  })
```

## Methods
* `new SheetsAPI(props [opt]) → {function}`
  * ### Get/Set data
    * `addRange(range, values) → {promise}`
    * `batchGetRange(ranges, sheetName [opt]) → {promise}`
    * `clearRange(range) → {promise}`
    * `getRange(range) → {promise}`
    * `setRange(range, values) → {promise}`
    * `setCell(cell, value) → {promise}`
  * ### Set options
    * `goToId(spreadsheetId) → {promise}`
    * `goToSheet(sheetName) → {promise}`
  * ### Aliases
    * `batchGetCell(cells, sheetName [opt]) → {promise}` (`batchGetRange`)
    * `clearCell(cell) → {promise}` (`clearRange`)
    * `getCell(cell) → {promise}` (`getRange`)


## Helpers

Helpers allow keeping a shallow queue. They return the sheet methods and allow setting a callback

```js
import SheetsAPI from 'node-google-sheets-api'
import {getRange, setRange, setCell, clearRange, toJSON} from 'node-google-sheets-api/helpers'

const sheetsAPI = new SheetsAPI({
  scopes: ['https://www.googleapis.com/auth/spreadsheets']
})

sheetsAPI()
  .then(sheets => sheets.goToId('<SHEET_ID>'))
  .then(sheets => sheets.goToSheet('<SHEET_NAME>'))
  .then(sheets => clearRange(sheets, 'A1:C5'))
  .then(sheets => setRange(sheets, 'A1:C1', [['Name', 'Email address', 'Telephone number']]))
  .then(sheets => setRange(sheets, 'A2:C2', [['Marge Simpson', 'marge@simpson.com', '123456789']]))
  .then(sheets => setRange(sheets, 'A3:C3', [['Homer Simpson', 'homer@simpson.com', '123456789']]))
  .then(sheets => setCell(sheets, 'A2', 'Maggie Simpson'))
  .then(sheets => getRange(sheets, 'A1:C4', response => {
    const data = toJSON(response.values)
    console.log(JSON.stringify(data))
    // [{
    // "name": "Marge Simpson",
    // "emailAddress": "marge@simpson.com",
    // "telephoneNumber": "123456789",
    // },{
    // "name": "Homer Simpson",
    // "emailAddress": "homer@simpson.com",
    // "telephoneNumber": "123456789",
    // },{
    // "name": "Maggie Simpson"
    // }]
  }))
  .catch(err => {
    throw err
  })
```


## Options

* If you want to store your files in a different location you can configure their relative path.
* Per default the access is readonly. To get write access set a different scope (without ".readonly" suffix)

```js
import SheetsAPI from 'node-google-sheets-api'

const sheetsAPI = new SheetsAPI({
  clientSecret: './google/client_secret.json',
  serviceAccount: './google/service_account.json',
  scopes: ['https://www.googleapis.com/auth/spreadsheets']
})

sheetsAPI()
  .then(sheets => sheets.goToId('<SHEET_ID>'))
  .then(sheets => sheets.goToSheet('<SHEET_NAME>'))
  .then(sheets => sheets.setCell('A2', 'This is cell A2'))
  .then(response => {
    console.log(reponse.values)
  })
```
